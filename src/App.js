import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Route, Switch, BrowserRouter } from "react-router-dom";

import Login from "./components/login";
import Home from "./components/home";
import Attendance from "./components/attendance";
import NotFound from "./components/not-found";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Login} />
        {/* <Route path="/home/:id" component={Home} /> */}
        <Route path="/home" component={Home} />
        <Route path="/attendance" component={Attendance} />
        <Route path="*" component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
