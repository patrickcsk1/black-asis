import firebase from "firebase";

export const actualizarContrasenaUsuario = async (idUsuario, newContrasena) => {
  var dato = await firebase
    .firestore()
    .collection("usuarios")
    .doc(idUsuario)
    .update({
      contrasena: newContrasena,
      unavez: 2
    });
  return dato;
};

export const agregarUsuario = async (
  uidNU,
  nombreNU,
  apellidoNU,
  edadNU,
  direccionNU,
  telefonoNU,
  fechaIn,
  usuarioNU,
  rolNU,
  contraNU
) => {
  var dato = await firebase
    .firestore()
    .collection("usuarios")
    .add({
      uid: uidNU,
      nombre: nombreNU,
      apellido: apellidoNU,
      edad: edadNU,
      direccion: direccionNU,
      telefono: telefonoNU,
      fechaIngreso: fechaIn,
      usuario: usuarioNU,
      rol: rolNU,
      contrasena: contraNU,
      eliminado: 0,
      unavez: 1,
      login: false
    });
  return dato;
};

export const agregarMiembro = async (
  nombreNU,
  apellidoNU,
  edadNU,
  direccionNU,
  telefonoNU,
  fechaIn,
  usuarioNU,
  uidLider
) => {
  var dato = await firebase
    .firestore()
    .collection("usuarios")
    .add({
      nombre: nombreNU,
      apellido: apellidoNU,
      edad: edadNU,
      direccion: direccionNU,
      telefono: telefonoNU,
      fechaIngreso: fechaIn,
      usuario: usuarioNU,
      rol: "miembro",
      liderUID: uidLider,
      eliminado: 0
    });
  return dato;
};

export const agregarAsistencia = async (
  diaID,
  fechaId,
  miembroId,
  uidLider
) => {
  firebase
    .firestore()
    .collection("asistencia")
    .add({
      idDia: diaID,
      idFecha: fechaId,
      idMember: miembroId,
      uidLider: uidLider,
      asis: false
    });
};

export const logoutAx = idUsuario => {
  firebase
    .firestore()
    .collection("usuarios")
    .doc(idUsuario)
    .update({
      login: false
    });
};

export const loginAx = idUsuario => {
  firebase
    .firestore()
    .collection("usuarios")
    .doc(idUsuario)
    .update({
      login: true
    });
};

export const obtenerUsuarios = async () => {
  var lista = await firebase
    .firestore()
    .collection("usuarios")
    .orderBy("nombre")
    .get();
  return lista;
};

export const obtenerDias = async () => {
  var lista = await firebase
    .firestore()
    .collection("dias")
    .orderBy("descripcion")
    .get();
  return lista;
};

export const obtenerFechas = async () => {
  var lista = await firebase
    .firestore()
    .collection("fechas")
    .get();
  return lista;
};

export const obtenerAsistencias = async () => {
  var lista = await firebase
    .firestore()
    .collection("asistencia")
    .get();
  return lista;
};

export const obtenerUsuario = async (usuario, contrasena) => {
  var user = await firebase
    .firestore()
    .collection("usuarios")
    .where("usuario", "==", usuario)
    .where("contrasena", "==", contrasena)
    .get();
  return user.docs[0];
};

export const obtenerUsuarioByUid = async uidUsuario => {
  var user = await firebase
    .firestore()
    .collection("usuarios")
    .where("uid", "==", uidUsuario)
    .get();
  return user.docs[0];
};

export const obtenerUsuarioById = async idUsuario => {
  var user = await firebase
    .firestore()
    .collection("usuarios")
    .doc(idUsuario)
    .get();
  return user;
};

export const obtenerFecha = (fechaIn, idDiaIn) => {
  firebase
    .firestore()
    .collection("fechas")
    .where("fecha", "==", fechaIn)
    .where("idDia", "==", idDiaIn)
    .get();
};

export const agregarFecha = (fechaIn, idDiaIn) => {
  firebase
    .firestore()
    .collection("fechas")
    .add({
      fecha: fechaIn,
      idDia: idDiaIn
    });
};

export const obtenerDia = (dia, idUsuarioLider) => {
  firebase
    .firestore()
    .collection("fechas")
    .where("fecha", "==", dia)
    .where("idDia", "==", idUsuarioLider)
    .get();
};

export const agregarDia = async (dia, idUsuarioLider) => {
  var dato = await firebase
    .firestore()
    .collection("dias")
    .add({
      descripcion: dia,
      idLider: idUsuarioLider
    });
  return dato;
};

export const eliminarUsuario = async id => {
  var dato = await firebase
    .firestore()
    .collection("usuarios")
    .doc(id)
    .update({
      eliminado: 1
    });
  return dato;
};

export const actualizarUsuario = async (
  idUpdate,
  nombreNU,
  apellidoNU,
  edadNU,
  direccionNU,
  telefonoNU
) => {
  var updatedUser = await firebase
    .firestore()
    .collection("usuarios")
    .doc(idUpdate)
    .update({
      nombre: nombreNU,
      apellido: apellidoNU,
      edad: edadNU,
      direccion: direccionNU,
      telefono: telefonoNU
    });
  return updatedUser;
};

export const tomarAsistencia = async (id, checkedValue) => {
  var updateAsistencia = await firebase
    .firestore()
    .collection("asistencia")
    .doc(id)
    .update({ asis: checkedValue });
  return updateAsistencia;
};
