import axios from "axios";
// const path_base = "http://localhost:4000/api/";
const path_base = "http://52.201.232.250:4000/api/";

// ================================ USERS ================================ \\

export const createUser = (
  token,
  nombre,
  apellido,
  direccion,
  edad,
  fechaIngreso,
  telefono,
  usuario,
  contrasena,
  eliminado,
  unavez,
  liderId,
  login
) => {
  var url = path_base + "usuarios/create";
  var body = {
    nombre,
    apellido,
    direccion,
    edad,
    fechaIngreso,
    telefono,
    usuario,
    contrasena,
    eliminado,
    unavez,
    liderId,
    login
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const createUserRol = (token, idUsuario, idRol) => {
  var url = path_base + "usuarios/createUserRol";
  var body = {
    idUsuario,
    idRol
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const deleteUser = (token, idUsuario) => {
  var url = path_base + "usuarios/delete";
  var body = {
    idUsuario
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.patch(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const getUserById = (token, idUsuario) => {
  var url = path_base + "usuarios/usuario";
  var body = {
    idUsuario
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const getUsuarioRolId = (token, idUsuario, idRol) => {
  var url = path_base + "usuarios/usuarioRol";
  var body = {
    idUsuario, idRol
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const getUsers = (token, opcion, id) => {
  var url = path_base + "usuarios/all";
  var body = {
    opcion,
    id
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  // console.log(body);
  return axios.post(url, body, config).then(res => {
    return res.data;
  });
};

export const login = (usuario, contrasena) => {
  var url = path_base + "usuarios/login";
  var body = {
    usuario,
    contrasena
  };
  return axios.post(url, body).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const updateLogin = (token, login, idUsuario) => {
  var url = path_base + "usuarios/updateLogin";
  var body = {
    login,
    idUsuario
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.patch(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const updatePass = (token, contrasena, idUsuario) => {
  var url = path_base + "usuarios/contrasena";
  var body = {
    contrasena,
    idUsuario
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.patch(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const updateUser = (
  token,
  nombre,
  apellido,
  edad,
  direccion,
  telefono,
  idUsuario
) => {
  var url = path_base + "usuarios/update";
  var body = {
    nombre,
    apellido,
    edad,
    direccion,
    telefono,
    idUsuario
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.patch(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

// ================================ ROLES ================================ \\

export const getRoles = token => {
  var url = path_base + "roles/all";
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.get(url, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

// ================================ FECHAS ================================ \\

export const getFechas = (token, idUsuario, idRol) => {
  var url = path_base + "fechas/all";
  var body = {
    idUsuario, idRol
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const fechaValid = (token, idDia, fecha) => {
  var url = path_base + "fechas/fechaValid";
  var body = {
    idDia,
    fecha
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const createFecha = (token, idDia, fecha) => {
  var url = path_base + "fechas/create";
  var body = {
    idDia,
    fecha
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

// ================================ DIAS ================================ \\

export const getDias = (token, idLider) => {
  var url = path_base + "dias/all";
  var body = {
    idLider
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const createDia = (token, dia, idLider) => {
  var url = path_base + "dias/create";
  var body = {
    dia, idLider
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const diaValid = (token, idLider, dia) => {
  var url = path_base + "dias/diaValid";
  var body = {
    idLider, dia
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

// ================================ ASISTENCIAS ================================ \\

export const createAsistencia = (token, asis, idFecha, idMiembro) => {
  var url = path_base + "asistencias/create";
  var body = {
    asis, idFecha, idMiembro
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const getAsistencias = (token, idDia) => {
  var url = path_base + "asistencias/all";
  var body = {
    idDia
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.post(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

export const takeAsistencia = (token, asis, idAsistencia) => {
  var url = path_base + "asistencias/takeList";
  var body = {
    asis, idAsistencia
  };
  var config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.patch(url, body, config).then(res => {
    // console.log("RES: ", JSON.stringify(res));
    return res.data;
  });
};

// ================================ AUXILIARES ================================ \\

export const obtenerDiaEs = dia => {
  if (dia === "Mon") return "lunes";
  else if (dia === "Tue") return "martes";
  else if (dia === "Wed") return "miercoles";
  else if (dia === "Thu") return "jueves";
  else if (dia === "Fri") return "viernes";
  else if (dia === "Sat") return "sabado";
  else if (dia === "Sun") return "domingo";
};