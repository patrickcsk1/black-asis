import React, { Component } from "react";
import { getUserById, updateLogin } from "./../servicios/backend";
import swal from "sweetalert2";
import Navbar from "./navbar";
import Lider from "./lider";
import Admin from "./admin";
import "../styles/home.css";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.mounted = false;

    this.state = {
      idUsuarioLog: this.props.location === {} ? this.props.location.state.idUsuario : "",
      token: this.props.location === {} ? this.props.location.state.token : "",
      usuarioLog: null
    };
  }

  componentDidMount() {
    if (this.props.location.state === undefined) this.props.history.push("/");
    else {
      var idUser = this.props.location.state.idUsuario;
      var token = this.props.location.state.token;
      this.setState({ token, idUsuarioLog: idUser });
      getUserById(token, idUser).then(res => {
        // console.log(res);
        if (res.success === 1) {
          this.setState({ usuarioLog: res.data });
          if (res.data.eliminado === 1 || res.data.login === 0)
            this.props.history.push("/");
        } else this.props.history.push("/");
      });
    }
  }

  logout = () => {
    // console.log(this.state);
    updateLogin(this.state.token, 0, this.state.usuarioLog.idUsuarios).then(
      respLogin => {
        // console.log(respLogin);
        if (respLogin.success === 1) this.props.history.push("/");
        else
          swal.fire({
            title: "Error",
            text: "Fallo al cerrar sesión",
            icon: "error"
          });
      }
    );
  };

  render() {
    if (this.state.usuarioLog === null)
      return (
        <div className="container" style={{ marginTop: "30%" }}>
          <h1 style={{ color: "white", display: "block" }}>Cargando</h1>
          <div className="spinner-border text-light" role="status"></div>
        </div>
      );

    const esAdmin = this.state.usuarioLog.idRoles === 1; //1 Administrado 2 Lider 3 Miembro

    return (
      <div className="container-home">
        <Navbar
          nombre={this.state.usuarioLog.nombre}
          clickeo={() => this.logout()}
        />
        <div style={{ margin: "5px" }}>
          {esAdmin ? (
            <Admin usuario={this.state.usuarioLog} token={this.state.token} />
          ) : 
          // null
          <Lider usuario={this.state.usuarioLog} token={this.state.token} />
          }
        </div>
      </div>
    );
  }
}
