import React, { Component } from "react";
import "../styles/navbar.css";
import logo from "../assets/logo.jpg";

export default class Navbar extends Component {
  render() {
    return (
      <div className="nav-def">
        <nav className="nav-bar">
          <div className="row">
            {/* <div className="col centrado" style={{paddingLeft:"calc(70px + 4vh)"}}> */}
            <div className="col-3 centrado">
              <img className="img-nav" src={logo} alt="Logo"></img>
            </div>
            <div className="col-5">
              <h1 className="titulo1"> Bienvenido(a) {this.props.nombre}</h1>
            </div>
            <div className="col margenDer">
              <label onClick={this.props.clickeo} className="puntero"> <u>Cerrar Sesión</u></label>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}
