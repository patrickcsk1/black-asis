import React, { Component } from "react";
import swal from "sweetalert2";
import "../styles/attendance.css";
import Navbar from "./navbar";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {
  createAsistencia,
  createDia,
  createFecha,
  diaValid,
  fechaValid,
  getUsers,
  getFechas,
  getAsistencias,
  obtenerDiaEs,
  takeAsistencia,
  updateLogin,
  getUsuarioRolId
  // createUser,
  // createUserRol,
  // getDias,
  // getRoles,
} from "./../servicios/backend";

export default class Attendance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flag: "",
      diaSelected: "",
      token: "",
      idUsuarioRol: 0,
      lider: null,
      miembros: [],
      fechas: [],
      asistencias: [],
      startDate: new Date(),
      lista: [],
      idFecha: 0
    };
  }

  handleCheckBox(e, index) {
    var auxLista = [...this.state.lista];
    auxLista[index].asis = e.target.checked;
    this.setState({ lista: auxLista });
  }

  handleChangeDias = dateIn => {
    this.setState({
      startDate: dateIn
    });
  };

  getFechas = async token => {
    await getFechas(
      token,
      this.state.lider.idUsuarios,
      this.state.lider.idRoles
    ).then(resf => {
      if (resf.success === 1) {
        console.log("GetFechas:", resf);
        var lista = [];
        resf.data.forEach(elem => {
          if (elem.idDia === this.state.diaSelected) lista.push(elem);
        });
        console.log("GetFechas:", lista);
        this.setState({ fechas: lista });
      }
    });
  };

  cargarAsistencias = async token => {
    await getAsistencias(token, this.state.diaSelected).then(resga => {
      // console.log(resga);
      if (resga.success === 1) {
        this.setState({ asistencias: resga.data });
      }
    });
  };

  getMiembros = async (token, idUsuarioRol) => {
    await getUsers(token, "lider", idUsuarioRol).then(resUs => {
      // console.log("resUs", resUs);
      if (resUs.success === 1) this.setState({ miembros: resUs.data });
    });
  };

  async componentDidMount() {
    this._ismounted = true;
    if (this.props.location.state === undefined) this.props.history.push("/");
    else {
      const { data } = this.props.location.state;
      console.log(data);
      if (!data) this.props.history.push("/home");
      else {
        this.setState({
          flag: data.flag,
          diaSelected: data.idDia,
          lider: data.lider,
          token: data.token,
          idUsuarioRol: data.idUsuarioRol
        });
        if (this._ismounted) {
          await this.getMiembros(data.token, data.idUsuarioRol);

          var auxLista = [...this.state.lista];
          this.state.miembros.forEach(miembro => {
            var objLista = {
              miembro,
              asis: false
            };
            auxLista.push(objLista);
          });

          this.setState({ lista: auxLista });
          console.log(this.state.lista);
          if (data.flag > 0) {
            await this.getFechas(data.token);
            await this.cargarAsistencias(data.token);
          }
        }
      }
    }
  }

  componentWillUnmount() {
    this._ismounted = false;
  }

  swalWindow = (titulo, texto, icono, opcion) => {
    if (opcion === 1) {
      return swal.fire({
        title: titulo,
        text: texto,
        icon: icono,
        timer: 12000
      });
    } else if (opcion === 2) {
      return swal.fire({
        title: titulo,
        text: texto,
        icon: icono,
        confirmButtonColor: "#00032a",
        confirmButtonText: "Aceptar"
      });
    } else if (opcion === 3) {
      return swal.fire({
        title: titulo,
        text: texto,
        icon: icono,
        showCancelButton: true,
        confirmButtonColor: "#00032a",
        cancelButtonColor: "#e32a58",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar"
      });
    }
  };

  convertirFechaNumber = fecha => {
    var arr = fecha.split("/");
    return Number(arr[2]) * 10000 + Number(arr[1]) * 100 + Number(arr[0]);
  };

  headersFecha = () => {
    return (this.state.fechas.length === 1
      ? this.state.fechas
      : this.state.fechas.sort((a, b) => {
          if (
            this.convertirFechaNumber(a.fecha) >
            this.convertirFechaNumber(b.fecha)
          )
            return 1;
          else if (
            this.convertirFechaNumber(a.fecha) <
            this.convertirFechaNumber(b.fecha)
          )
            return -1;
          else return 0;
        })
    ).map((currentDate, i) => {
      return <th key={i}>{currentDate.fecha}</th>;
    });
  };

  tomarAsistencia = (check, miembro, fecha) => {
    var checkedValue = check ? 1 : 0;
    var dato = this.state.asistencias.filter(
      asistencia =>
        asistencia.idFechas === fecha.idFechas &&
        asistencia.idDia === fecha.idDia &&
        asistencia.idUsuarios === miembro.idUsuarios
    );
    // console.log(checkedValue, dato);
    if (dato.length > 0) {
      // console.log("LAAAA", dato[0].asis, checkedValue);
      if (dato[0].asis !== checkedValue) {
        // console.log("BBBBBBBBBB");
        swal
          .fire({
            title: "Aviso",
            text: "Seguro que quieres modificar la asistencia?",
            icon: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonText: "Aceptar"
          })
          .then(async result => {
            // console.log(result);
            if (result.value) {
              await takeAsistencia(
                this.state.token,
                checkedValue,
                dato[0].idAsistencias
              ).then(restake => {
                // console.log(restake);
                if (restake.success === 1) {
                  var index = this.state.asistencias
                    .map(data => {
                      return data.idAsistencias;
                    })
                    .indexOf(dato[0].idAsistencias);
                  var auxAsistencia = [...this.state.asistencias];
                  if (index !== -1) {
                    var objeto = auxAsistencia[index];
                    objeto.asis = checkedValue;
                    auxAsistencia[index] = objeto;
                    this.setState({
                      asistencias: auxAsistencia
                    });
                    swal.fire({
                      title: "Aviso",
                      text: "Operación exitosa",
                      icon: "success"
                    });
                  }
                } else {
                  console.log("Error takeAsistencia: ", restake.message);
                }
              });
            }
          });
      }
    }
  };

  valorDefault = (miembro, fecha) => {
    var dato = this.state.asistencias.filter(
      asistencia =>
        asistencia.idFechas === fecha.idFechas &&
        asistencia.idDia === fecha.idDia &&
        asistencia.idUsuarios === miembro.idUsuarios
    );
    if (dato.length > 0) return dato[0].asis === 0 ? false : true;
    else return false;
  };

  auxFunctionBox = member => {
    return this.state.fechas.map((currentDate, i) => {
      return (
        <td key={i}>
          <input
            type="checkbox"
            checked={this.valorDefault(member, currentDate)}
            onChange={e =>
              this.tomarAsistencia(e.target.checked, member, currentDate)
            }
          ></input>
        </td>
      );
    });
  };

  bodyMembers = () => {
    return this.state.miembros.map((currentMember, i) => {
      return (
        <tr key={i}>
          <td className="ancho-col">
            {currentMember.nombre + " " + currentMember.apellido}
          </td>
          {this.auxFunctionBox(currentMember)}
        </tr>
      );
    });
  };

  getCantFaltas = fecha => {
    var listaFiltrada = this.state.asistencias.filter(
      asistencia =>
        asistencia.idFecha === fecha.idFechas && asistencia.asis === 0
    );
    return listaFiltrada.length;
  };

  totalFaltas = () => {
    return this.state.fechas.map((currentDate, i) => {
      return <td key={i}>{this.getCantFaltas(currentDate)}</td>;
    });
  };

  resumenFechas = () => {
    return (
      <tr>
        <td>Total Faltas: </td>
        {this.totalFaltas()}
      </tr>
    );
  };

  bodyTable = () => {
    var fin = this.resumenFechas();
    return <tbody>{fin}</tbody>;
  };

  logout = () => {
    // console.log(this.state);
    updateLogin(this.state.token, 0, this.state.lider.idUsuarios).then(
      respLogin => {
        // console.log(respLogin);
        if (respLogin.success === 1) this.props.history.push("/");
        else
          swal.fire({
            title: "Error",
            text: "Fallo al cerrar sesión",
            icon: "error"
          });
      }
    );
  };

  volverHome = () => {
    this.props.history.push({
      pathname: "/home",
      state: {
        idUsuario: this.state.lider.idUsuarios,
        token: this.state.token
      }
    });
  };

  bodyAgregarAsistencia = () => {
    return this.state.lista.map((elemento, i) => {
      var index = ("0" + (i + 1)).slice(-2);
      return (
        <tr key={elemento}>
          <td>{index}</td>
          <td className="ancho-col">
            {elemento.miembro.nombre + " " + elemento.miembro.apellido}
          </td>
          <td>
            <input
              type="checkbox"
              checked={elemento.asis}
              onChange={e => this.handleCheckBox(e, i)}
            ></input>
          </td>
        </tr>
      );
    });
  };

  // listo
  vistaAgregarAsistencia() {
    return (
      <div
        style={{
          marginTop: `calc(5px + 1.2vh)`,
          marginBottom: `calc(5px + 1.2vh)`
        }}
      >
        <div className="row">
          <div className="col-md-3">
            <label className="fuente">Dia de la celula:</label>
          </div>
          <div className="col-md-6">
            <DatePicker
              className="fecha"
              name="startDate"
              selected={this.state.startDate}
              onChange={this.handleChangeDias}
              dateFormat="dd/MM/yyyy"
              placeholderText="Seleccione dia"
            />
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-9">
            <div className="table-responsive">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th className="ancho-col">Nro</th>
                    <th className="ancho-col">Nombre</th>
                    <th className="ancho-col">Asistencia</th>
                  </tr>
                </thead>
                <tbody>{this.bodyAgregarAsistencia()}</tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }

  vistaDetallesAsistencia() {
    return (
      <div className="table-responsive">
        <table className="table table-hover mi-tabla">
          <thead>
            <tr>
              <th className="ancho-col">Nombre</th>
              {this.headersFecha()}
            </tr>
          </thead>
          {this.bodyTable()}
        </table>
      </div>
    );
  }

  vistaModificarAsistencia() {
    return (
      <div className="table-responsive">
        <table className="table table-hover mi-tabla">
          <thead>
            <tr>
              <th className="ancho-col">Nombre</th>
              {this.headersFecha()}
            </tr>
          </thead>
          <tbody>
            {this.state.miembros.length !== 0 ? (
              this.bodyMembers()
            ) : (
              <tr>
                <td colSpan={this.state.fechas.length + 1}>
                  <div style={{ textAlign: "center" }}>
                    No hay miembros registrados
                  </div>
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    );
  }

  // listo
  guardarFecha = async (fechaIn, idDiaIn) => {
    await fechaValid(this.state.token, idDiaIn, fechaIn).then(
      async resFecha => {
        if (resFecha.success === 1 && resFecha.data.length === 0) {
          await createFecha(this.state.token, idDiaIn, fechaIn).then(rescf => {
            if (rescf.success === 1) {
              console.log("Correcto: ", idDiaIn, fechaIn, rescf);
              this.setState({ idFecha: rescf.data.insertId });
            } else console.log("Error: " + rescf.message, idDiaIn, fechaIn);
          });
        } else {
          this.swalWindow("Aviso", `Fecha repetida: ${fechaIn}`, "warning", 1);
        }
      }
    );
  };

  // listo
  guardarAsistencia = async elemento => {
    await getUsuarioRolId(
      this.state.token,
      elemento.miembro.idUsuarios,
      elemento.miembro.idRoles
    ).then(async resId => {
      var asis = elemento.asis ? 1 : 0;
      console.log("Input Guardar asistencia", resId, elemento);
      //idMiembro corresponde al id que ocupa en la tabla Usuarios_has_Roles
      await createAsistencia(
        this.state.token,
        asis,
        this.state.idFecha,
        resId.data.idUsuarioRol
      ).then(resAsis => {
        if (resAsis.success !== 1)
          console.log("Error de asistencia: ", resAsis.message);
      });
    });
  };

  // listo
  addNewAsistencia = async () => {
    this.swalWindow(
      "Aviso",
      "Seguro que quiere agregar la asistencia?",
      "warning",
      3
    ).then(async result => {
      if (result.value) {
        console.log(this.state.lista);
        var dateIn = this.state.startDate;
        var auxDia = dateIn.toString().split(" ")[0];
        var dia = obtenerDiaEs(auxDia);
        var resDia = await diaValid(
          this.state.token,
          this.state.idUsuarioRol,
          dia
        );
        console.log(resDia);
        // return;
        if (resDia.success === 1) {
          var idDia = 0;
          if (resDia.data.length === 0) {
            // es un nuevo dia que entra.
            var rescd = await createDia(
              this.state.token,
              dia,
              this.state.idUsuarioRol
            );
            if (rescd.success === 1) {
              idDia = rescd.data.insertId;
            } else {
              this.swalWindow("Aviso", "Error al guardar dia", "error", 1);
              return;
            }
          } else {
            // es un dia repetido entonces solo se guardara la fecha.
            idDia = resDia.data[0].idDias;
          }

          var shortDate = dateIn.toLocaleString("es-ES", {
            dateStyle: "short"
          });
          console.log(shortDate);
          await this.guardarFecha(shortDate, idDia);

          this.state.lista.forEach(async elemento => {
            await this.guardarAsistencia(elemento);
          });

          this.swalWindow("Aviso", "Registro exitoso", "success", 1);
        } else {
          this.swalWindow("Aviso", "Error al validar dia", "error", 1);
          return;
        }
      }
    });
  };

  render() {
    if (this.state.flag === 1 && this.state.asistencias.length === 0)
      return (
        <div className="container" style={{ marginTop: "30%" }}>
          <h1 style={{ color: "white", display: "block" }}>Cargando</h1>
          <div className="spinner-border text-light" role="status"></div>
        </div>
      );

    return (
      <div>
        <Navbar
          nombre={this.props.location.state.data.lider.nombre}
          clickeo={() => this.logout()}
        />
        <div className="container-att">
          <h1 className="uno-color titulo">
            {this.state.flag === 0
              ? "Agregar Asistencia de Miembros"
              : this.state.flag === 1
              ? "Detalle de Asistencias"
              : "Editar Asistencia"}
          </h1>
          <hr />
          {
            {
              0: this.vistaAgregarAsistencia(),
              1: this.vistaDetallesAsistencia(),
              2: this.vistaModificarAsistencia()
            }[this.state.flag]
          }
          <button
            type="button"
            className="btn volver-btn"
            onClick={this.volverHome}
          >
            Volver
          </button>
          {this.state.flag === 0 ? (
            <button
              type="button"
              className="btn addNew-btn"
              onClick={this.addNewAsistencia}
            >
              Guardar
            </button>
          ) : null}
        </div>
      </div>
    );
  }
}
