import React, { Component } from "react";
import swal from "sweetalert2";
import ReactModal from "react-modal";
// import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import {
  // createAsistencia,
  createUser,
  createUserRol,
  getUsers,
  getDias,
  getFechas,
  getUsuarioRolId,
  deleteUser,
  getUserById,
  updateUser
  // getRoles,
  // obtenerDiaEs,
  // diaValid,
  // createDia,
  // fechaValid,
  // createFecha
} from "./../servicios/backend";

import "../styles/lider.css";

class Lider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dias: [],
      usuarios: [],
      fechas: [],
      token: this.props.token,
      usuario: this.props.usuario,
      idUserRol: 0,
      showModal: false,
      showModalUp: false,
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: "",
      usuarioNU: "",
      idUpdate: ""
    };
  }

  init = async () => {
    await getUsuarioRolId(
      this.state.token,
      this.state.usuario.idUsuarios,
      this.state.usuario.idRoles
    ).then(async resIdUR => {
      if (resIdUR.success === 1) {
        this.setState({ idUserRol: resIdUR.data.idUsuarioRol });
      }
    });
  };

  async componentDidMount() {
    await this.init();
    await this.getUsuarios();
    await this.getDias();
    await this.getFechas();
  }

  getUsuarios = async () => {
    await getUsers(this.state.token, "lider", this.state.idUserRol).then(
      resUs => {
        if (resUs.success === 1) this.setState({ usuarios: resUs.data });
      }
    );
  };

  getDias = async () => {
    await getDias(this.state.token, this.state.idUserRol).then(resDias => {
      if (resDias.success === 1) this.setState({ dias: resDias.data });
    });
  };

  getFechas = async () => {
    await getFechas(
      this.state.token,
      this.state.usuario.idUsuarios,
      this.state.usuario.idRoles
    ).then(resf => {
      if (resf.success === 1) {
        this.setState({ fechas: resf.data });
      }
    });
  };

  handleChange = e => {
    const value = e.target.value;
    this.setState({ ...this.state, [e.target.name]: value });
  };

  openModal = () => {
    this.setState({ showModal: true });
  };

  openModalUp = id => {
    // console.log("UPDATE USER");
    // console.log(id);
    var obj = this.state.usuarios.find(user => user.idUsuarios === id);
    this.setState({
      idUpdate: id,
      showModalUp: true,
      nombreNU: obj.nombre,
      apellidoNU: obj.apellido,
      edadNU: obj.edad,
      direccionNU: obj.direccion,
      telefonoNU: obj.telefono
    });
  };

  closeModal = () => {
    this.setState({
      showModal: false,
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: "",
      usuarioNU: ""
    });
  };

  closeModalUp = () => {
    this.setState({
      showModalUp: false,
      idUpdate: "",
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: "",
      usuarioNU: ""
    });
  };

  verificarCorreoExistente = () => {
    var auxExiste =
      this.state.usuarios.some(
        item => this.state.usuarioNU === item.usuario && item.eliminado === 0
      ) || this.state.usuario.usuario === this.state.usuarioNU;
    return auxExiste;
  };

  validarRegex = email => {
    // var regexContra = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{6,12}$/;
    var regexEmail = /^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,63}$/i;
    return email.toLowerCase().match(regexEmail);
  };

  swalWindow = (titulo, texto, icono, opcion) => {
    if (opcion === 1) {
      return swal.fire({
        title: titulo,
        text: texto,
        icon: icono,
        timer: 12000
      });
    } else if (opcion === 2) {
      return swal.fire({
        title: titulo,
        text: texto,
        icon: icono,
        confirmButtonColor: "#00032a",
        confirmButtonText: "Aceptar"
      });
    } else if (opcion === 3) {
      return swal.fire({
        title: titulo,
        text: texto,
        icon: icono,
        showCancelButton: true,
        confirmButtonColor: "#00032a",
        cancelButtonColor: "#e32a58",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar"
      });
    }
  };

  // guardarAsistencia = async idMiembro => {
  //   //idMiembro corresponde al id que ocupa en la tabla Usuarios_has_Roles
  //   await this.state.fechas.map(async fecha => {
  //     await createAsistencia(
  //       this.state.token,
  //       0,
  //       fecha.idFechas,
  //       idMiembro
  //     ).then(resAsis => {
  //       // console.log("resAsis: ", resAsis);
  //       if (resAsis.success !== 1)
  //         console.log("Error de asistencia: ", resAsis.message, fecha);
  //     });
  //   });
  // };

  addUser = async () => {
    if (this.verificarCorreoExistente()) {
      this.swalWindow("Error", "Miembro ingresado ya existe", "error", 1);
    } else {
      if (
        this.state.apellidoNU !== "" ||
        this.state.nombreNU !== "" ||
        this.state.direccionNU !== "" ||
        this.state.edadNU !== "" ||
        this.state.telefonoNU !== "" ||
        this.state.contraNU !== ""
      ) {
        if (this.state.edadNU > 9 && this.state.edadNU < 100) {
          if (this.validarRegex(this.state.usuarioNU)) {
            var fechaIn = new Date();
            fechaIn = fechaIn.toLocaleString("es-ES", {
              dateStyle: "short",
              timeStyle: "short"
            });
            var estados = this.state;
            createUser(
              this.state.token,
              this.state.nombreNU,
              this.state.apellidoNU,
              this.state.direccionNU,
              this.state.edadNU,
              fechaIn,
              this.state.telefonoNU,
              this.state.usuarioNU.toLowerCase(),
              "",
              0, //eliminado 0-> no 1 -> si
              1, //unavez 2-> no 1 -> si
              this.state.idUserRol, //liderId de tabla UsuarioRol
              0 //login 0-> no 1 -> si
            ).then(resCreate => {
              // console.log("resCreate: ", resCreate);
              if (resCreate.success === 1) {
                createUserRol(
                  estados.token,
                  resCreate.data.insertId,
                  3 // es rol de miembro
                  // ).then(async resUR => {
                ).then(resUR => {
                  // console.log("ESTADOS", estados);
                  // console.log("resUR: ", resUR);
                  if (resUR.success === 1) {
                    //guardo asistencia
                    // await this.guardarAsistencia(resUR.data.insertId);

                    //agrego al usuario ingresado a la lista de miembros
                    var auxUsuarios = [...this.state.usuarios];
                    getUserById(estados.token, resCreate.data.insertId).then(
                      resGet => {
                        // console.log("resGet: ", resGet);
                        if (resGet.success === 1) {
                          auxUsuarios.push(resGet.data);
                          this.setState({ usuarios: auxUsuarios });
                          this.swalWindow(
                            "Aviso",
                            "Registro Exitoso",
                            "success",
                            1
                          );
                          this.closeModal();
                        } else {
                          this.swalWindow("Error", resGet.message, "error", 1);
                        }
                      }
                    );
                  }
                });
              }
            });
          } else {
            this.swalWindow("Aviso", "Email invalido", "warning", 2);
          }
        } else {
          this.swalWindow("Aviso", "Edad ingresada invalida", "warning", 2);
        }
      } else {
        this.swalWindow(
          "Aviso",
          "Verifique que todos los campos estén completados",
          "warning",
          2
        );
      }
    }
  };

  deleteUser = id => {
    // console.log("DELETE USER: " + id);
    this.swalWindow(
      "Aviso",
      "Seguro que quiere eliminar al usuario?",
      "warning",
      3
    ).then(async result => {
      if (result.value) {
        deleteUser(this.state.token, id).then(resDel => {
          // console.log(resDel);
          if (resDel.success === 1) {
            var index = this.state.usuarios
              .map(usuario => {
                return usuario.idUsuarios;
              })
              .indexOf(id);
            var auxUsuarios = [...this.state.usuarios];
            if (index !== -1) {
              auxUsuarios.splice(index, 1);
              this.setState({ usuarios: auxUsuarios });
              this.swalWindow("Aviso", "Borrado exitoso", "success", 1);
            }
          } else {
            this.swalWindow("Aviso", resDel.message, "error", 1);
          }
        });
      }
    });
  };

  updateUser = async () => {
    if (
      this.state.apellidoNU !== "" ||
      this.state.nombreNU !== "" ||
      this.state.direccionNU !== "" ||
      this.state.edadNU !== "" ||
      this.state.telefonoNU !== ""
    ) {
      if (this.state.edadNU > 9 && this.state.edadNU < 100) {
        updateUser(
          this.state.token,
          this.state.nombreNU,
          this.state.apellidoNU,
          this.state.edadNU,
          this.state.direccionNU,
          this.state.telefonoNU,
          this.state.idUpdate
        ).then(resUp => {
          if (resUp.success === 1) {
            var index = this.state.usuarios
              .map(user => {
                return user.idUsuarios;
              })
              .indexOf(this.state.idUpdate);
            var auxUsuarios = [...this.state.usuarios];
            if (index !== -1) {
              auxUsuarios[index].nombre = this.state.nombreNU;
              auxUsuarios[index].apellido = this.state.apellidoNU;
              auxUsuarios[index].edad = this.state.edadNU;
              auxUsuarios[index].direccion = this.state.direccionNU;
              auxUsuarios[index].telefono = this.state.telefonoNU;
              this.setState({ usuarios: auxUsuarios });
              this.swalWindow("Aviso", "Actualizacion correcto", "success", 1);
              this.closeModalUp();
            }
          } else {
            this.swalWindow(
              "Aviso",
              "Fallo en la actualización del usuario",
              "error",
              1
            );
          }
        });
      } else {
        this.swalWindow("Aviso", "Edad ingresada invalida", "warning", 2);
      }
    } else {
      this.swalWindow(
        "Aviso",
        "Verifique que todos los campos estén completados",
        "warning",
        2
      );
    }
  };

  usuarioList = () => {
    return this.state.usuarios.map((currentusuario, i) => {
      var index = ("0" + (i + 1)).slice(-2);
      return (
        <tr key={i}>
          <td>{index}</td>
          <td>
            {currentusuario.nombre +
              " " +
              (currentusuario.apellido ? currentusuario.apellido : "")}
          </td>
          <td>{currentusuario.usuario}</td>
          <td>
            <button
              type="button"
              className="btn eliminar-btn"
              onClick={() => {
                this.deleteUser(currentusuario.idUsuarios);
              }}
            >
              Eliminar
            </button>
          </td>
          <td>
            <button
              type="button"
              className="btn actualizar-btn"
              onClick={() => {
                this.openModalUp(currentusuario.idUsuarios);
              }}
            >
              Actualizar
            </button>
          </td>
        </tr>
      );
    });
  };

  goAttendance = (flag, id = 0) => {
    if (this.state.usuarios.length === 0) {
      swal.fire({
        title: "Aviso",
        text: "Debe ingresar un usuario por lo menos",
        icon: "warning"
      });
    } else {
      if (flag === 2) {
        //modificar asistencia
        this.props.history.push({
          pathname: "/attendance",
          state: {
            data: {
              idDia: id,
              token: this.state.token,
              flag: 2,
              idUsuarioRol: this.state.idUserRol,
              lider: this.state.usuario
            }
          }
        });
      } else if (flag === 1) {
        //detalles de asistencia
        this.props.history.push({
          pathname: "/attendance",
          state: {
            data: {
              idDia: id,
              token: this.state.token,
              flag: 1,
              idUsuarioRol: this.state.idUserRol,
              lider: this.state.usuario
            }
          }
        });
      } else if (flag === 0) {
        //agregar asistencia
        this.props.history.push({
          pathname: "/attendance",
          state: {
            data: {
              idDia: id,
              token: this.state.token,
              flag: 0,
              idUsuarioRol: this.state.idUserRol,
              lider: this.state.usuario
            }
          }
        });
      }
    }
  };

  diasList = () => {
    const diasRows = this.state.dias.map((currentDay, i) => {
      return (
        <tr key={i}>
          <td>{currentDay.descripcion}</td>
          <td>
            <button
              type="button"
              className="btn detalle-btn"
              onClick={() => this.goAttendance(1, currentDay.idDias)}
            >
              Detalles
            </button>
          </td>
          <td>
            <button
              type="button"
              className="btn asistencia-btn"
              onClick={() => this.goAttendance(2, currentDay.idDias)}
            >
              Modificar Asistencia
            </button>
          </td>
        </tr>
      );
    });
    return <tbody>{diasRows}</tbody>;
  };

  render() {
    return (
      // <div className="container-lider">HOLA</div>
      <div className="container-lider">
        <div className="row">
          <div className="col-9">
            <h1 className="uno-color titulo">Lista de Miembros</h1>
          </div>
          <div className="col-3">
            <label className="efecto-hover" onClick={this.openModal}>
              <u>Agregar Miembro</u>
            </label>
          </div>
        </div>
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.showModal}
          contentLabel="Add user"
          style={{ height: "25%", width: "25%" }}
        >
          <div className="modal-div">
            <h1 className="uno-color titulo">Registro de Nuevo Miembro</h1>
            <hr />
            <div className="row">
              <div className="col-md-6">
                <label className="fuente">Nombre: </label>
                <input
                  type="text"
                  required
                  name="nombreNU"
                  className="form-control"
                  value={this.state.nombreNU}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-md-6">
                <label className="fuente">Apellido: </label>
                <input
                  type="text"
                  required
                  name="apellidoNU"
                  className="form-control"
                  value={this.state.apellidoNU}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <label className="fuente sangria">Dirección: </label>
              <input
                type="text"
                required
                name="direccionNU"
                className="form-control sangria"
                value={this.state.direccionNU}
                onChange={this.handleChange}
              />
            </div>
            <div className="row">
              <div className="col-md-6">
                <label className="fuente">Edad: </label>
                <input
                  type="number"
                  required
                  name="edadNU"
                  className="form-control"
                  value={this.state.edadNU}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-md-6">
                <label className="fuente">Teléfono: </label>
                <input
                  type="text"
                  required
                  name="telefonoNU"
                  className="form-control"
                  value={this.state.telefonoNU}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <label className="fuente sangria">Email: </label>
              <input
                type="text"
                required
                name="usuarioNU"
                className="form-control sangria"
                value={this.state.usuarioNU}
                onChange={this.handleChange}
              />
            </div>
            <div className="row botones">
              <button
                type="button"
                className="btn guardar-btn"
                onClick={this.addUser}
              >
                Guardar
              </button>
              <button
                type="button"
                className="btn salir-btn"
                onClick={this.closeModal}
              >
                Cerrar
              </button>
            </div>
          </div>
        </ReactModal>
        <div className="table-responsive">
          <table className="table table-hover table-sm">
            <thead>
              <tr>
                <th>Nro</th>
                <th>Nombre</th>
                <th>Usuario</th>
                <th>Eliminar</th>
                <th>Update</th>
              </tr>
            </thead>
            <tbody>
              {this.state.usuarios.length !== 0 ? (
                this.usuarioList()
              ) : (
                <tr>
                  <td colSpan="5">
                    <div style={{ textAlign: "center" }}>
                      No hay usuarios registrados
                    </div>
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.showModalUp}
          contentLabel="Update user"
          style={{ height: "25%", width: "25%" }}
        >
          <div className="modal-div">
            <h1 className="uno-color">Actualizar Miembro</h1>
            <hr />
            <div className="row">
              <label className="fuente sangria">Nombre: </label>
              <input
                type="text"
                required
                name="nombreNU"
                className="form-control sangria"
                defaultValue={this.state.nombreNU || ""}
                onChange={this.handleChange}
              />
            </div>
            <div className="row">
              <label className="fuente sangria">Apellido: </label>
              <input
                type="text"
                required
                name="apellidoNU"
                className="form-control sangria"
                defaultValue={this.state.apellidoNU || ""}
                onChange={this.handleChange}
              />
            </div>
            <div className="row">
              <div className="col-6">
                <label className="fuente">Edad: </label>
                <input
                  type="number"
                  required
                  name="edadNU"
                  className="form-control"
                  defaultValue={this.state.edadNU || 0}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-6">
                <label className="fuente">Telefono: </label>
                <input
                  type="text"
                  required
                  name="telefonoNU"
                  className="form-control"
                  defaultValue={this.state.telefonoNU || ""}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <label className="fuente sangria">Dirección: </label>
              <input
                type="text"
                required
                name="direccionNU"
                className="form-control sangria"
                defaultValue={this.state.direccionNU || ""}
                onChange={this.handleChange}
              />
            </div>
            <div className="row botones">
              <button
                type="button"
                className="btn guardar-btn"
                onClick={this.updateUser}
              >
                Guardar
              </button>

              <button
                type="button"
                className="btn salir-btn"
                onClick={this.closeModalUp}
              >
                Salir
              </button>
            </div>
          </div>
        </ReactModal>

        <h1 className="uno-color titulo">Dias de Célula</h1>
        <br />
        <div className="table-responsive">
          <table className="table table-hover table-sm">
            <thead>
              <tr>
                <th>Dia</th>
                <th>Detalles</th>
                <th>Modificar</th>
              </tr>
            </thead>
            {this.state.dias.length !== 0 ? (
              this.diasList()
            ) : (
              <tbody>
                <tr>
                  <td colSpan="3">
                    <div style={{ textAlign: "center" }}>
                      No hay dias registrados
                    </div>
                  </td>
                </tr>
              </tbody>
            )}
          </table>
        </div>
        <div style={{ textAlign: "center" }}>
          <button
            type="button"
            className="btn addAsistencia-btn"
            onClick={() => this.goAttendance(0)}
          >
            Agregar Asistencia
          </button>
        </div>
      </div>
    );
  }
}

export default withRouter(Lider);
