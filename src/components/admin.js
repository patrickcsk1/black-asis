import React, { Component } from "react";
import swal from "sweetalert2";
import ReactModal from "react-modal";
import {
  getUsers,
  getRoles,
  deleteUser,
  updateUser,
  createUser,
  createUserRol,
  getUserById
  // obtenerDiaEs,
  // diaValid,
  // createDia,
  // fechaValid,
  // createFecha
} from "./../servicios/backend";

import "../styles/admin.css";

// import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // startDate: new Date(),
      // cantSesiones: 0,
      usuario: this.props.usuario,
      token: this.props.token,
      usuarios: [],
      roles: [],
      showModal: false,
      showModalUp: false,
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: "",
      usuarioNU: "",
      rolNU: 1,
      contraNU: "",
      contraNU2: "",
      idUpdate: ""
    };
  }

  cargarUsuarios = async () => {
    var resUs = await getUsers(
      this.state.token,
      "admin",
      this.state.usuario.idUsuarios
    );
    if (resUs.success === 1) this.setState({ usuarios: resUs.data });
  };

  cargarRoles = async () => {
    await getRoles(this.state.token).then(resRol => {
      if (resRol.success === 1) this.setState({ roles: resRol.data });
    });
  };

  async componentDidMount() {
    await this.cargarUsuarios();
    await this.cargarRoles();
  }

  handleChange = e => {
    if (e.target.name === "rolNU") {
      var num = Number(e.target.value);
      this.setState({ ...this.state, [e.target.name]: num });
    } else {
      const value = e.target.value;
      this.setState({ ...this.state, [e.target.name]: value });
    }
  };

  // handleChangeDias = dateIn => {
  //   this.setState({
  //     startDate: dateIn
  //   });
  // };

  swalWindow = (titulo, texto, icono, opcion) => {
    if (opcion === 1) {
      return swal.fire({
        title: titulo,
        text: texto,
        icon: icono,
        timer: 12000
      });
    } else if (opcion === 2) {
      return swal.fire({
        title: titulo,
        text: texto,
        icon: icono,
        confirmButtonColor: "#00032a",
        confirmButtonText: "Aceptar"
      });
    } else if (opcion === 3) {
      return swal.fire({
        title: titulo,
        text: texto,
        icon: icono,
        showCancelButton: true,
        confirmButtonColor: "#00032a",
        cancelButtonColor: "#e32a58",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar"
      });
    }
  };

  // guardarFecha = async (fechaIn, idDiaIn) => {
  //   await fechaValid(this.state.token, idDiaIn, fechaIn).then(
  //     async resFecha => {
  //       if (resFecha.success === 1 && resFecha.data.length === 0) {
  //         await createFecha(this.state.token, idDiaIn, fechaIn).then(rescf => {
  //           if (rescf.success === 1)
  //             console.log("Correcto: ", idDiaIn, fechaIn);
  //           else console.log("Error: " + rescf.message, idDiaIn, fechaIn);
  //         });
  //       } else {
  //         this.swalWindow("Aviso", `Fecha repetida: ${fechaIn}`, "warning", 1);
  //       }
  //     }
  //   );
  // };

  // confirmarClases = async (idUsuarioLider, cantSesiones) => {
  //   var dateIn = this.state.startDate;
  //   var auxDia = dateIn.toString().split(" ")[0];
  //   var dia = obtenerDiaEs(auxDia);

  //   await diaValid(this.state.token, idUsuarioLider, dia).then(async resVal => {
  //     if (resVal.success === 1 && resVal.data.length === 0) {
  //       await createDia(this.state.token, dia, idUsuarioLider).then(
  //         async rescd => {
  //           // var token = this.state.token;
  //           if (rescd.success === 1) {
  //             var idDia = rescd.data.insertId;
  //             for (let i = 0; i < cantSesiones; i++) {
  //               var shortDate = dateIn.toLocaleString("es-ES", {
  //                 dateStyle: "short"
  //               });
  //               console.log(shortDate);
  //               await this.guardarFecha(shortDate, idDia);
  //               dateIn.setDate(dateIn.getDate() + 7);
  //             }
  //             this.swalWindow("Aviso", "Registro exitoso", "success", 1);
  //           } else {
  //             this.swalWindow("Aviso", "Dia repetido", "warning", 1);
  //             return;
  //           }
  //         }
  //       );
  //     } else this.swalWindow("Aviso", "Dia repetido", "warning", 1);
  //   });
  // };

  openModal = () => {
    this.setState({ showModal: true, showModalUp: false });
  };

  openModalUp = id => {
    var obj = this.state.usuarios.find(user => user.idUsuarios === id);
    this.setState({
      idUpdate: id,
      showModalUp: true,
      showModal: false,
      nombreNU: obj.nombre,
      apellidoNU: obj.apellido,
      edadNU: obj.edad,
      direccionNU: obj.direccion,
      telefonoNU: obj.telefono
    });
  };

  closeModal = () => {
    this.setState({
      showModal: false,
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: "",
      usuarioNU: "",
      rolNU: "admin",
      contraNU: "",
      contraNU2: "",
      startDate: new Date(),
      cantSesiones: 0
    });
  };

  closeModalUp = () => {
    this.setState({
      showModalUp: false,
      idUpdate: "",
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: ""
    });
  };

  verificarCorreoExistente = () => {
    var auxExiste =
      this.state.usuarios.some(
        item => this.state.usuarioNU === item.usuario && item.eliminado === 0
      ) || this.state.usuario.usuario === this.state.usuarioNU;
    return auxExiste;
  };

  validarRegex = (contra, email) => {
    var regexContra = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{6,12}$/;
    var regexEmail = /^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,63}$/i;
    if (this.state.contraNU === "------") return true;
    return contra.match(regexContra) && email.toLowerCase().match(regexEmail);
  };

  addUser = () => {
    if (this.verificarCorreoExistente()) {
      this.swalWindow("Error", "Usuario ingresado ya existe", "error", 1);
    } else {
      if (
        this.state.apellidoNU !== "" ||
        this.state.nombreNU !== "" ||
        this.state.direccionNU !== "" ||
        this.state.edadNU !== "" ||
        this.state.telefonoNU !== "" ||
        this.state.contraNU !== ""
      ) {
        if (this.state.edadNU > 9 && this.state.edadNU < 100) {
          if (this.state.contraNU === this.state.contraNU2) {
            if (this.validarRegex(this.state.contraNU, this.state.usuarioNU)) {
              var fechaIn = new Date();
              fechaIn = fechaIn.toLocaleString("es-ES", {
                dateStyle: "short",
                timeStyle: "short"
              });
              var estados = this.state;
              createUser(
                this.state.token,
                this.state.nombreNU,
                this.state.apellidoNU,
                this.state.direccionNU,
                this.state.edadNU,
                fechaIn,
                this.state.telefonoNU,
                this.state.usuarioNU.toLowerCase(),
                this.state.contraNU,
                0, //eliminado 0-> no 1 -> si
                1, //unavez 2-> no 1 -> si
                "", //liderId
                0 //login 0-> no 1 -> si
              )
                .then(resCreate => {
                  if (resCreate.success === 1) {
                    createUserRol(
                      estados.token,
                      resCreate.data.insertId,
                      estados.rolNU
                    ).then(async resUR => {
                      if (resUR.success === 1) {
                        // if (estados.rolNU === 2) {
                        //   await this.confirmarClases(
                        //     resUR.data.insertId,
                        //     estados.cantSesiones
                        //   );
                        // }
                        var auxUsuarios = [...this.state.usuarios];
                        getUserById(
                          estados.token,
                          resCreate.data.insertId
                        ).then(resGet => {
                          if (resGet.success === 1) {
                            auxUsuarios.push(resGet.data);
                            this.setState({ usuarios: auxUsuarios });
                            this.swalWindow(
                              "Aviso",
                              "Registro Exitoso",
                              "success",
                              1
                            );
                            this.closeModal();
                          } else {
                            this.swalWindow(
                              "Error",
                              resGet.message,
                              "error",
                              1
                            );
                          }
                        });
                      } else {
                        this.swalWindow("Error", resUR.message, "error", 1);
                      }
                    });
                  } else {
                    this.swalWindow("Error", resCreate.message, "error", 1);
                  }
                })
                .catch(error => {
                  console.log(error.message);
                  this.swalWindow("Error", error.message, "error", 1);
                });
              this.closeModal();
            } else {
              this.swalWindow(
                "Aviso",
                "Formato de Email/Contraseña invalido",
                "warning",
                2
              );
            }
          } else {
            this.swalWindow("Aviso", "Contraseñas no coinciden", "warning", 2);
          }
        } else {
          this.swalWindow("Aviso", "Edad ingresada invalida", "warning", 2);
        }
      } else {
        this.swalWindow(
          "Aviso",
          "Verifique que todos los campos estén completados",
          "warning",
          2
        );
      }
    }
  };

  deleteUser = id => {
    this.swalWindow(
      "Aviso",
      "Seguro que quiere eliminar al usuario?",
      "warning",
      3
    ).then(result => {
      if (result.value) {
        deleteUser(this.state.token, id).then(resDel => {
          // console.log(resDel);
          if (resDel.success === 1) {
            var index = this.state.usuarios
              .map(usuario => {
                return usuario.idUsuarios;
              })
              .indexOf(id);
            var auxUsuarios = [...this.state.usuarios];
            if (index !== -1) {
              auxUsuarios.splice(index, 1);
              this.setState({ usuarios: auxUsuarios });
              this.swalWindow("Aviso", "Borrado exitoso", "success", 1);
            }
          } else {
            this.swalWindow("Aviso", resDel.message, "error", 1);
          }
        });
      }
    });
  };

  updateUser = () => {
    if (
      this.state.apellidoNU !== "" ||
      this.state.nombreNU !== "" ||
      this.state.direccionNU !== "" ||
      this.state.edadNU !== "" ||
      this.state.telefonoNU !== ""
    ) {
      if (this.state.edadNU > 9 && this.state.edadNU < 100) {
        updateUser(
          this.state.token,
          this.state.nombreNU,
          this.state.apellidoNU,
          this.state.edadNU,
          this.state.direccionNU,
          this.state.telefonoNU,
          this.state.idUpdate
        ).then(resUp => {
          if (resUp.success === 1) {
            var index = this.state.usuarios
              .map(user => {
                return user.idUsuarios;
              })
              .indexOf(this.state.idUpdate);
            var auxUsuarios = [...this.state.usuarios];
            if (index !== -1) {
              auxUsuarios[index].nombre = this.state.nombreNU;
              auxUsuarios[index].apellido = this.state.apellidoNU;
              auxUsuarios[index].edad = this.state.edadNU;
              auxUsuarios[index].direccion = this.state.direccionNU;
              auxUsuarios[index].telefono = this.state.telefonoNU;
              this.setState({ usuarios: auxUsuarios });
              this.swalWindow("Aviso", "Actualizacion correcto", "success", 1);
              this.closeModalUp();
            }
          } else {
            this.swalWindow(
              "Aviso",
              "Fallo en la actualización del usuario",
              "error",
              1
            );
          }
        });
      } else {
        this.swalWindow("Aviso", "Edad ingresada invalida", "warning", 2);
      }
    } else {
      this.swalWindow(
        "Aviso",
        "Verifique que todos los campos estén completados",
        "warning",
        2
      );
    }
  };

  usuarioList = () => {
    return this.state.usuarios.map((currentusuario, i) => {
      var index = ("0" + (i + 1)).slice(-2);
      return (
        <tr key={i}>
          <td>{index}</td>
          <td>
            {currentusuario.nombre +
              " " +
              (currentusuario.apellido ? currentusuario.apellido : "")}
          </td>
          <td>{currentusuario.usuario}</td>
          <td>{currentusuario.descripcion}</td>
          <td>
            <button
              type="button"
              className="btn eliminar-btn"
              onClick={() => {
                this.deleteUser(currentusuario.idUsuarios);
              }}
            >
              Delete
            </button>
          </td>
          <td>
            <button
              type="button"
              className="btn actualizar-btn"
              onClick={() => {
                this.openModalUp(currentusuario.idUsuarios);
              }}
            >
              Update
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div className="container-admin">
        <div className="row">
          <div className="col-9">
            <h1 className="uno-color titulo">Lista de Usuarios</h1>
          </div>
          <div className="col-3">
            <label onClick={this.openModal} className="efecto-hover">
              <u>Agregar Usuario</u>
            </label>
          </div>
        </div>
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.showModal}
          contentLabel="Add user"
          style={{ height: "25%", width: "25%" }}
        >
          <div className="modal-div">
            <h1 className="uno-color titulo">Registro de Nuevo Miembro</h1>
            <hr />
            <div className="row">
              <div className="col-md-6">
                <label className="fuente">Nombre: </label>
                <input
                  type="text"
                  required
                  name="nombreNU"
                  className="form-control"
                  value={this.state.nombreNU}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-md-6">
                <label className="fuente">Apellido: </label>
                <input
                  type="text"
                  required
                  name="apellidoNU"
                  className="form-control"
                  value={this.state.apellidoNU}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <label className="fuente sangria">Dirección: </label>
              <input
                type="text"
                required
                name="direccionNU"
                className="form-control sangria"
                value={this.state.direccionNU}
                onChange={this.handleChange}
              />
            </div>
            <div className="row">
              <div className="col-md-4">
                <label className="fuente">Edad: </label>
                <input
                  type="number"
                  required
                  name="edadNU"
                  className="form-control"
                  value={this.state.edadNU}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-md-4">
                <label className="fuente">Teléfono: </label>
                <input
                  type="text"
                  // required
                  name="telefonoNU"
                  className="form-control"
                  value={this.state.telefonoNU}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-md-4">
                <label className="fuente">Seleccione el rol:</label>
                <select
                  className="form-control"
                  value={this.state.rolNU}
                  name="rolNU"
                  onChange={this.handleChange}
                >
                  {this.state.roles.map(rol =>
                    rol.idRoles === 3 ? null : (
                      <option key={rol.idRoles} value={rol.idRoles}>
                        {rol.descripcion}
                      </option>
                    )
                  )}
                </select>
              </div>
            </div>
            <div className="row">
              <label className="fuente sangria">Email: </label>
              <input
                type="text"
                required
                name="usuarioNU"
                className="form-control sangria"
                value={this.state.usuarioNU}
                onChange={this.handleChange}
              />
            </div>
            <div className="row">
              <div className="col-md-6">
                <label className="fuente">Contraseña: </label>
                <input
                  type="password"
                  required
                  name="contraNU"
                  className="form-control"
                  value={this.state.contraNU}
                  onChange={this.handleChange}
                />
                <span style={{ color: "red", fontSize: "10px" }}>
                  Ingrese contraseña
                </span>
              </div>
              <div className="col-md-6">
                <label className="fuente">Repetir Contraseña: </label>
                <input
                  type="password"
                  required
                  name="contraNU2"
                  className="form-control"
                  value={this.state.contraNU2}
                  onChange={this.handleChange}
                />
                <span style={{ color: "red", fontSize: "10px" }}>
                  Valide contraseña
                </span>
              </div>
            </div>
            {/* {this.state.rolNU === 2 ? (
              <div className="row">
                <div className="col-md-6">
                  <label className="fuente">
                    Seleccione el dia inicial de la celula:
                  </label>
                  <DatePicker
                    className="fecha"
                    name="startDate"
                    selected={this.state.startDate}
                    onChange={this.handleChangeDias}
                    dateFormat="dd/MM/yyyy"
                    placeholderText="Seleccione dia inicial"
                  />
                </div>
                <div className="col-md-6">
                  <label className="fuente">Cantidad de sesiones:</label>
                  <input
                    type="number"
                    required
                    placeholder="Nro sesiones"
                    name="cantSesiones"
                    className="form-control"
                    value={this.state.cantSesiones}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
            ) : null} */}
            <div className="row botones">
              <button
                type="button"
                className="btn guardar-btn"
                onClick={this.addUser}
              >
                Guardar
              </button>
              <button
                type="button"
                className="btn salir-btn"
                onClick={this.closeModal}
              >
                Cerrar
              </button>
            </div>
          </div>
        </ReactModal>
        <div className="table-responsive scroll">
          <table className="table table-hover table-sm">
            <thead>
              <tr>
                <th>Nro</th>
                <th>Nombre</th>
                <th>Usuario</th>
                <th>Rol</th>
                <th>Eliminar</th>
                <th>Update</th>
              </tr>
            </thead>
            <tbody>
              {this.state.usuarios.length !== 0 ? (
                this.usuarioList()
              ) : (
                <tr>
                  <td colSpan="6">
                    <div style={{ textAlign: "center" }}>
                      No hay usuarios registrados
                    </div>
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.showModalUp}
          contentLabel="Update user"
          style={{ height: "25%", width: "25%" }}
        >
          <div className="modal-div">
            <h1 className="uno-color titulo">Actualizar Usuario</h1>
            <hr />
            <div className="row">
              <label className="fuente sangria">Nombre: </label>
              <input
                type="text"
                required
                name="nombreNU"
                className="form-control sangria"
                defaultValue={this.state.nombreNU || ""}
                onChange={this.handleChange}
              />
            </div>
            <div className="row">
              <label className="fuente sangria">Apellido: </label>
              <input
                type="text"
                required
                name="apellidoNU"
                className="form-control sangria"
                defaultValue={this.state.apellidoNU || ""}
                onChange={this.handleChange}
              />
            </div>
            <div className="row">
              <div className="col-md-6">
                <label className="fuente">Edad: </label>
                <input
                  type="number"
                  required
                  name="edadNU"
                  className="form-control"
                  defaultValue={this.state.edadNU || 0}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-md-6">
                <label className="fuente">Telefono: </label>
                <input
                  type="text"
                  required
                  name="telefonoNU"
                  className="form-control"
                  defaultValue={this.state.telefonoNU || ""}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <label className="fuente sangria">Dirección: </label>
              <input
                type="text"
                required
                name="direccionNU"
                className="form-control sangria"
                defaultValue={this.state.direccionNU || ""}
                onChange={this.handleChange}
              />
            </div>
            <div className="row botones">
              <button
                type="button"
                className="btn guardar-btn"
                onClick={this.updateUser}
              >
                Guardar
              </button>
              <button
                type="button"
                className="btn salir-btn"
                onClick={this.closeModalUp}
              >
                Salir
              </button>
            </div>
          </div>
        </ReactModal>
      </div>
    );
  }
}
