import React, { Component } from "react";
import swal from "sweetalert2";
import "../styles/login.css";
import logo from "../assets/logo.jpg";
import {
  login,
  updateLogin,
  updatePass,
  getUserById
} from "./../servicios/backend";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      usuario: "",
      contrasena: ""
    };
  }

  onChange = e => {
    const value = e.target.value;
    this.setState({ ...this.state, [e.target.name]: value });
  };

  onKeyDown = e => {
    if (e.key === "Enter") {
      e.preventDefault();
      e.stopPropagation();
      this.logueo();
    }
  };

  validarRegex = contra => {
    var regexContra = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{6,12}$/;
    // var regexEmail = /^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,63}$/i;
    return contra.match(regexContra);
  };

  errorMessage = mensaje => {
    swal.fire({
      title: "Error",
      text: mensaje,
      icon: "error"
    });
  };

  logueo = () => {
    if (this.state.usuario === "" || this.state.contrasena === "") {
      this.errorMessage("Error en logueo: Campos vacios");
    } else {
      login(this.state.usuario, this.state.contrasena).then(resLogin => {
        // console.log(resLogin);
        if (resLogin.success === 1) {
          //logueo exitoso
          updateLogin(resLogin.token, 1, resLogin.idUsuario).then(
            resUpdLogin => {
              if (resUpdLogin.success === 1) {
                getUserById(resLogin.token, resLogin.idUsuario)
                  .then(resUser => {
                    if (resUser.success === 1) {
                      //hallo usuario
                      if (resUser.data.unavez === 1) {
                        swal.fire({
                          title: "Ingrese nueva contraseña",
                          input: "password",
                          inputPlaceholder: "Ingrese nueva contraseña",
                          showCancelButton: true,
                          confirmButtonColor: "#00032a",
                          cancelButtonColor: "#e32a58",
                          inputValidator: value => {
                            if (!value) {
                              return "Ingrese contraseña!";
                            }
                            if (this.validarRegex(value)) {
                              updatePass(
                                resLogin.token,
                                value,
                                resLogin.idUsuario
                              ).then(resPass => {
                                if (resPass.success === 1) {
                                  this.props.history.push({
                                    pathname: "/home",
                                    state: {
                                      idUsuario: resLogin.idUsuario,
                                      token: resLogin.token
                                    }
                                  });
                                } else {
                                  this.errorMessage(
                                    "Error en logueo: " + resPass.message
                                  );
                                }
                              });
                            } else {
                              return "Formato de contraseña invalido!";
                            }
                          }
                        });
                      } else {
                        this.props.history.push({
                          pathname: "/home",
                          state: {
                            idUsuario: resLogin.idUsuario,
                            token: resLogin.token
                          }
                        });
                      }
                    } else {
                      this.errorMessage("Error en logueo: " + resUser.message);
                    }
                  })
                  .catch(e => console.log("UserById: ", e.message));
              } else {
                this.setState({ usuario: "", contrasena: "" });
                this.errorMessage("Error en logueo: " + resUpdLogin.message);
              }
            }
          );
        } else {
          this.setState({ usuario: "", contrasena: "" });
          this.errorMessage("Error en logueo: " + resLogin.message);
        }
      });
    }
  };

  render() {
    return (
      <div className="container container-login">
        <div className="centro">
          <div className="row separa">
            <img src={logo} alt="Logo"></img>
          </div>
          <div className="row centro">
            <h3 className="titulo">Bienvenido</h3>
          </div>
          <div className="row separa centro">
            <input
              type="text"
              required
              className="form-control entradas"
              placeholder="Correo"
              name="usuario"
              value={this.state.usuario}
              onChange={this.onChange}
            />
          </div>
          <div className="row separa centro">
            <input
              type="password"
              required
              className="form-control entradas"
              placeholder="Contraseña"
              name="contrasena"
              value={this.state.contrasena}
              onChange={this.onChange}
              onKeyDown={this.onKeyDown}
            />
          </div>
          <div className="row separa grupo-botones">
            <button
              type="button"
              className="btn btn-primary"
              style={{ minWidth: "80%", width: "3%", marginTop: "15px" }}
              onClick={this.logueo}
            >
              Login
            </button>
          </div>
        </div>
      </div>
    );
  }
}
