import React, { Component } from "react";
import "../styles/not-found.css";

export default class NotFound extends Component {
  render() {
    return (
      <div id="notfound">
        <div class="notfound">
          <div class="notfound-404">
            <h1>404</h1>
            <h2>Error</h2>
          </div>
          <a href="/">Ir al Login</a>
        </div>
      </div>
    );
  }
}
